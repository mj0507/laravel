<?php

use App\Http\Controllers\CourseController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', function () {
    return view('home');
})->name('home');
/*
Route::get('/', function () {
    return view('welcome');
})->name('welcome');*/

Route::get('/user', [UserController::class, 'index'])->name('listeUsers');

Route::get('/liste', [CourseController::class, 'index'])->name('listeCours');
