@extends('layout')

@section('content')
<h1>Cours</h1>

<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Nom cours</th>
        <th scope="col">Code</th>
    </thead>
    <tbody>
        @foreach($courses as $course)
            <tr>
                <td>{{ $course->name }}</td>
                <td>{{ $course->code }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection
