<h1>Profil</h1>
<table class="table table-hover">
    <thead>
    <tr>
        <th>Email</th>
        <th>Utilisateur</th>
        <th>Création</th>
        <th>Dernière visite</th>
        <th>Admin</th>
    </tr>
    </thead>
    <tbody>

@foreach($user as $users)
    <tr>
        <td>{{ $users->email }}</td>
        <td>{{ $users->username }}</td>
        <td>{{ date_format(date_create($users->lastlogin),"d/m/Y à H:i:s") }}</td>
        <td>{{ date_format(date_create($users->created),"d/m/Y à H:i:s") }}</td>
        <td>{{ $users->admin ? 'oui' : 'non' }}</td>
    </tr>
@endforeach
    </tbody>
</table>
